using System;
using System.Linq;
using System.Collections.Generic;
using System.Net.Http.Headers;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Text.Json;
using Xunit;
using Xunit.Priority;

namespace IntegrationTests;

/// <summary>
/// Stores test user and access token for that user
/// </summary>
public static class TestData
{
    private static readonly string _userName = $"IntegrationTestUser{DateTime.Now.Ticks}";
    public static string UserName 
    {
        get {
            return _userName;
        }
    }
    public static string? AccessToken 
    {
        get;
        set;
    }
}

[TestCaseOrderer(PriorityOrderer.Name, PriorityOrderer.Assembly)]
public class Tests : IDisposable
{

    private readonly HttpClientHandler _handler;
    private readonly HttpClient _client;

    public Tests()
    {
        _handler = new HttpClientHandler();
        _client = new HttpClient(_handler);

        //Generally this should be pulled from some configuration
        _client.BaseAddress = new Uri("http://127.0.0.1:8100/api/");
    }

    public void Dispose()
    {
        _client.Dispose();
        _handler.Dispose();
    }

    /// <summary>
    /// Verify we can create a new user
    /// </summary>
    /// <returns></returns>
    [Fact, Priority(1)]
    public async Task New_User_Should_Be_Created()
    {
        var content = new FormUrlEncodedContent(new[]
        {
            new KeyValuePair<string, string>("name", TestData.UserName),
            new KeyValuePair<string, string>("email", $"{TestData.UserName}@email.com"),
            new KeyValuePair<string, string>("password", "Robert'); DROP TABLE Students;--"),
        });
        var response = await _client.PostAsync("user", content);
        Assert.Equal<HttpStatusCode>(HttpStatusCode.OK, response.StatusCode);

        var responseContent = await response.Content.ReadAsStringAsync();
        Assert.True(int.TryParse(responseContent, out _));
    }

    /// <summary>
    /// Verify we can authenticate using the newly created user
    /// </summary>
    /// <returns></returns>
    [Fact, Priority(2)]
    public async Task User_Should_Be_Authenticated_With_Correct_Password()
    {
        var content = new FormUrlEncodedContent(new[]
        {
            new KeyValuePair<string, string>("email", $"{TestData.UserName}@email.com"),
            new KeyValuePair<string, string>("password", "Robert'); DROP TABLE Students;--"),
        });
        var response = await _client.PostAsync("user/authenticate", content);
        Assert.Equal<HttpStatusCode>(HttpStatusCode.OK, response.StatusCode);

        var responseContent = await response.Content.ReadAsStringAsync();
        TestData.AccessToken = responseContent.Substring(responseContent.IndexOf(':') + 2, responseContent.LastIndexOf('"') - responseContent.IndexOf(':') - 2);

        //a possible future test could be validating jwt can be decoded
    }

    /// <summary>
    /// Verify the negative case for authenticating
    /// </summary>
    /// <returns></returns>
    [Fact]
    public async Task User_Should_Not_Be_Authenticated_With_Incorrect_Password()
    {
        var content = new FormUrlEncodedContent(new[]
        {
            new KeyValuePair<string, string>("email", $"{TestData.UserName}@email.com"),
            new KeyValuePair<string, string>("password", "Little Bobby Tables"),
        });
        var response = await _client.PostAsync("user/authenticate", content);

        Assert.Equal<HttpStatusCode>(HttpStatusCode.Unauthorized, response.StatusCode);
    }

    /// <summary>
    /// Verify that we can set Philly as a favorite
    /// </summary>
    /// <returns></returns>
    [Fact, Priority(10)]
    public async Task User_Should_Favorite_Philly()
    {
        var city = "Philadelphia, PA";
        var country = "USA";
        _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", TestData.AccessToken);
        var response = await _client.PostAsync($"cities/{city}/{country}/favorite", null);

        Assert.Equal<HttpStatusCode>(HttpStatusCode.OK, response.StatusCode);
    }

    /// <summary>
    /// Verify that we can set Boston as a favorite
    /// </summary>
    /// <returns></returns>
    [Fact, Priority(10)]
    public async Task User_Should_Favorite_Boston()
    {
        var city = "Boston, MA";
        var country = "USA";
        _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", TestData.AccessToken);
        var response = await _client.PostAsync($"cities/{city}/{country}/favorite", null);

        Assert.Equal<HttpStatusCode>(HttpStatusCode.OK, response.StatusCode);
    }

    /// <summary>
    /// Verify Philly comes back as a favorite
    /// </summary>
    /// <returns></returns>
    [Fact, Priority(20)]
    public async Task Users_Favorite_City_Should_Include_Philly()
    {
        _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", TestData.AccessToken);
        var response = await _client.GetAsync("cities/favorites");
        var responseContent = await response.Content.ReadAsStringAsync();

        Assert.Equal<HttpStatusCode>(HttpStatusCode.OK, response.StatusCode);
        Assert.Contains("Philadelphia", responseContent);
    }

    /// <summary>
    /// Verify Boston comes back as a favorite
    /// </summary>
    /// <returns></returns>
    [Fact, Priority(20)]
    public async Task Users_Favorite_City_Should_Include_Boston()
    {
        _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", TestData.AccessToken);
        var response = await _client.GetAsync("cities/favorites");
        var responseContent = await response.Content.ReadAsStringAsync();

        Assert.Equal<HttpStatusCode>(HttpStatusCode.OK, response.StatusCode);
        Assert.Contains("Boston", responseContent);
    }

    /// <summary>
    /// Verify we can Delete Boston as a favorite
    /// </summary>
    /// <returns></returns>
    [Fact, Priority(30)]
    public async Task Users_Should_Delete_Boston()
    {
        var city = "Boston, MA";
        var country = "USA";
        _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", TestData.AccessToken);
        var response = await _client.DeleteAsync($"cities/{city}/{country}/favorite");

        Assert.Equal<HttpStatusCode>(HttpStatusCode.OK, response.StatusCode);
    }

    /// <summary>
    /// Verify Boston no longer comes back as a favorite
    /// </summary>
    /// <returns></returns>
    [Fact, Priority(40)]
    public async Task Users_Favorite_City_Should_Not_Include_Boston()
    {
        _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", TestData.AccessToken);
        var response = await _client.GetAsync("cities/favorites");
        var responseContent = await response.Content.ReadAsStringAsync();

        Assert.Equal<HttpStatusCode>(HttpStatusCode.OK, response.StatusCode);
        Assert.DoesNotContain("Boston", responseContent);
    }

    /// <summary>
    /// Verify the negative case for favoriting a city, NYC was not set
    /// </summary>
    /// <returns></returns>
    [Fact, Priority(20)]
    public async Task Users_Favorite_City_Should_Not_Include_NYC()
    {
        _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", TestData.AccessToken);
        var response = await _client.GetAsync("cities/favorites");
        var responseContent = await response.Content.ReadAsStringAsync();

        Assert.Equal<HttpStatusCode>(HttpStatusCode.OK, response.StatusCode);
        Assert.DoesNotContain("New York", responseContent);
    }

    /// <summary>
    /// Verify we can set Tea as a favorite drink for a speakeasy
    /// </summary>
    /// <returns></returns>
    [Fact, Priority(10)]
    public async Task User_Should_Favorite_RandomTeaRoom()
    {
        var city = "Philadelphia, PA";
        var country = "USA";
        var speakeasyName = "The Random Tea Room";

        var content = new FormUrlEncodedContent(new[]
        {
            new KeyValuePair<string, string>("favoriteDrink", "Silver Needle"),
        });
        _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", TestData.AccessToken);
        var response = await _client.PostAsync($"cities/{city}/{country}/speakeasy/{speakeasyName}/favorite", content);

        Assert.Equal<HttpStatusCode>(HttpStatusCode.OK, response.StatusCode);
    }

    /// <summary>
    /// Verify Tea for a speakeasy comes back as a favorite drink
    /// </summary>
    /// <returns></returns>
    [Fact, Priority(20)]
    public async Task Users_Favorite_Speakeasies_Should_Include_Random()
    {
        var city = "Philadelphia, PA";
        var country = "USA";
        _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", TestData.AccessToken);
        var response = await _client.GetAsync($"cities/{city}/{country}/speakeasies/favorites");
        var responseContent = await response.Content.ReadAsStringAsync();

        Assert.Equal<HttpStatusCode>(HttpStatusCode.OK, response.StatusCode);
        Assert.Contains("The Random Tea Room", responseContent);
        Assert.Contains("Silver Needle", responseContent);
    }

    /// <summary>
    /// Verify the negative case for favoriting a speakeasy, Starbucks was not set
    /// </summary>
    /// <returns></returns>
    [Fact, Priority(20)]
    public async Task Users_Favorite_Speakeasies_Should_Not_Include_Starbucks()
    {
        var city = "Philadelphia, PA";
        var country = "USA";
        _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", TestData.AccessToken);
        var response = await _client.GetAsync($"cities/{city}/{country}/speakeasies/favorites");
        var responseContent = await response.Content.ReadAsStringAsync();

        Assert.Equal<HttpStatusCode>(HttpStatusCode.OK, response.StatusCode);
        Assert.DoesNotContain("Starbucks", responseContent);
        Assert.DoesNotContain("Coffee", responseContent);
    }

    /// <summary>
    /// Verify we can remove a favorite speakeasy
    /// </summary>
    /// <returns></returns>
    [Fact, Priority(30)]
    public async Task User_Should_Unfavorite_RandomTeaRoom()
    {
        var city = "Philadelphia, PA";
        var country = "USA";
        var speakeasyName = "The Random Tea Room";

        _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", TestData.AccessToken);
        var response = await _client.DeleteAsync($"cities/{city}/{country}/speakeasy/{speakeasyName}/favorite");

        Assert.Equal<HttpStatusCode>(HttpStatusCode.OK, response.StatusCode);
    }

    /// <summary>
    /// Verify previously favorited speakeasy is no longer a favorite
    /// </summary>
    /// <returns></returns>
    [Fact, Priority(40)]
    public async Task Users_Should_No_Logner_Favorite_RandomTeaRoom()
    {
        var city = "Philadelphia, PA";
        var country = "USA";
        _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", TestData.AccessToken);
        var response = await _client.GetAsync($"cities/{city}/{country}/speakeasies/favorites");
        var responseContent = await response.Content.ReadAsStringAsync();

        Assert.Equal<HttpStatusCode>(HttpStatusCode.OK, response.StatusCode);
        Assert.DoesNotContain("The Random Tea Room", responseContent);
    }

    /// <summary>
    /// Create ten users, set Dunkin Donuts as a favorite for each
    /// Verify Dunkin Donuts comes back as a favorite for at least nine users
    /// </summary>
    /// <returns></returns>
    [Fact, Priority(30)]
    public async Task Dunkin_Donuts_Should_Be_Most_Popular()
    {
        for (var i = 0; i < 10; i++) 
        {
            var content = new FormUrlEncodedContent(new[]
            {
                new KeyValuePair<string, string>("name", $"{i}-TestData.UserName"),
                new KeyValuePair<string, string>("email", $"{i}-{TestData.UserName}@email.com"),
                new KeyValuePair<string, string>("password", $"{i}-Password"),
            });
            var response = await _client.PostAsync("user", content);
            Assert.Equal<HttpStatusCode>(HttpStatusCode.OK, response.StatusCode);

            content = new FormUrlEncodedContent(new[]
            {
                new KeyValuePair<string, string>("email", $"{i}-{TestData.UserName}@email.com"),
                new KeyValuePair<string, string>("password", $"{i}-Password"),
            });
            response = await _client.PostAsync("user/authenticate", content);
            Assert.Equal<HttpStatusCode>(HttpStatusCode.OK, response.StatusCode);

            var responseContent = await response.Content.ReadAsStringAsync();
            var accessToken = responseContent.Substring(responseContent.IndexOf(':') + 2, responseContent.LastIndexOf('"') - responseContent.IndexOf(':') - 2);

            var city = "Philadelphia, PA";
            var country = "USA";
            var speakeasyName = "Dunkin Donuts";
            content = new FormUrlEncodedContent(new[]
            {
                new KeyValuePair<string, string>("favoriteDrink", "Wooder"),
            });
            _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
            response = await _client.PostAsync($"cities/{city}/{country}/speakeasy/{speakeasyName}/favorite", content);

            Assert.Equal<HttpStatusCode>(HttpStatusCode.OK, response.StatusCode);
        }

        var sharedResponse = await _client.GetAsync("speakeasy/sharedFavorites");
        var sharedResponseContent = await sharedResponse.Content.ReadAsStringAsync();

        Assert.Contains("Dunkin Donuts", sharedResponseContent);
        var jsonResponse = JsonDocument.Parse(sharedResponseContent);
        var count = jsonResponse.RootElement.EnumerateArray().Where(je => je.GetProperty("name").GetString() == "Dunkin Donuts").Single().GetProperty("count").GetInt32();
        Assert.True(count > 9);
    }


}