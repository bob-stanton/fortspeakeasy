# FORT Technical Exercise
Runtime was changed to .NET 6. 

After standing up the containers, navigate to swagger at http://127.0.0.1:8100/swagger/index.html 

Then execute `/api/admin/database/create` to create the *FortCodeDb* database, followed by `/api/admin/database/tables/create` to create the *User*, *City*, *Speakeasy*, *CityFavorite* and *SpeakeasyFavorite* tables.

Optionally execute `/api/admin/database/seed` to fill the database with some sample data. Create other users by executing `/api/user`. Grab a JWT by executing `/api/user/authenticate/`. Copy the token, scroll up to the top, click Authorize and paste in the token to use the City and Speakeasy APIs. If using seed data, try `bob@email.com` and `Password1`

If this was a real production system I'd likely keep the above in SQL script files, seperate utlity application, or some type of database migration tool. Embedding this in the API seemed like the most straightforward approach for this exercise. 

After the database has been stood up, integration tests can be run from the command line by navigating to the **IntegrationTests** folder, then execute `dotnet test`. Sixteen tests cover the User, City and Speakeasy APIs (Admin APIs are not included). 

If this was a more complex system (and not a get it done quickly test exercise), I'd probably build *Service* classes with mock *Repositories* and test those instead of (or, depending on the overall situation, in addition to) the integration tests. 
