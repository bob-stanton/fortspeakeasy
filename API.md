* `POST /api/user satisfies`: Create a user account with the following:

  * Name

  * Email

  * Password

* `POST /api/user/authenticate` satisfies: A user should be able to authenticate with an email and password

* `POST & DELETE /api/cities/{city}/{country}/favorite` with *Authorization* header `Bearer {token from /api/user/authenticate}` satisfies: An authenticated user should be able to add and remove their favorite cities where a city consists of the following:

    * City name

    * Country

* `GET /api/cities/favorites` with *Authorization* header `Bearer {token from /api/user/authenticate}` satisfies: An authenticated user should be able to retrieve a list of their favorite cities

* `POST & DELETE /api/cities/{city}/{country}/speakeasy/{speakeasyName}/favorite` with *Authorization* header `Bearer {token from /api/user/authenticate}` satisfies: An authenticated user should be able to add and remove secret bars (speakeasys) they patron in their favorite cities where a bar consists of the following:

    * Bar name

    * Favorite drink name

* `GET /api/cities/{cityName}/{countryName}/speakeasies/favorites` with *Authorization* header `Bearer {token from /api/user/authenticate}` satisfies: An authenticated user should be able to retrieve a list of their favorite bars in one of their favorite cities

* `GET /api/speakeasy/sharedFavorites` with *Authorization* header `Bearer {token from /api/user/authenticate}` satisfies: An authenticated user should be able to query if any of their favorite bars are shared with other users in the system by returning the shared bar names and the number of other users that share it