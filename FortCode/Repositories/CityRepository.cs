using Dapper;
using FORTSpeakeasy.Models;

namespace FORTSpeakeasy.Repositories;

public class CityRepository : RepositoryBase
{
   
    //if this project was larger, I'd consider wrapping some of this functionality into a service, but for the purpose of this test project I'd like to keep it simple
    private readonly UserRepository _userRepo;

    public CityRepository(IConfiguration configuration, UserRepository userRepo) : base(configuration) {
        _userRepo = userRepo;
    }
   
    /// <summary>
    /// Creates the specified city if it does not already exist
    /// </summary>
    /// <param name="city"></param>
    /// <param name="country"></param>
    /// <returns>CityId</returns>
    public async Task<int> CreateCity(string city, string country)
    {
        using (var connection = base.CreateConnection())
        {
            var cityIdQuery = "SELECT c.Id FROM [City] c WHERE c.Name=@Name and c.Country=@Country";
            var cityIdParams = new DynamicParameters();
            cityIdParams.Add("Name", city);
            cityIdParams.Add("Country", country);   

            var existingCityId = await connection.ExecuteScalarAsync<int?>(cityIdQuery, cityIdParams);
            if (!existingCityId.HasValue)
            {
                var insertCityQuery = "INSERT INTO [City] (Name, Country) OUTPUT INSERTED.Id VALUES (@Name, @Country);";
                existingCityId = await connection.ExecuteScalarAsync<int>(insertCityQuery, cityIdParams);
            }

            return existingCityId.Value;
        }

    }

    /// <summary>
    /// Sets the specified city as a favorite for user corresponding to email
    /// </summary>
    /// <param name="email"></param>
    /// <param name="city"></param>
    /// <param name="country"></param>
    /// <returns></returns>
    public async Task<bool> Favorite(string email, string city, string country)
    { 
        using (var connection = base.CreateConnection())
        {
            var userId = await _userRepo.GetUserId(email);
            var cityId = await CreateCity(city, country);

            var favoriteExistsQuery = 
                @"SELECT COUNT(1) 
                FROM [CityFavorite] cf
                WHERE cf.UserId=@UserId AND cf.CityId=@CityId";
            var favoriteExistsParams = new DynamicParameters();
            favoriteExistsParams.Add("UserId", userId);
            favoriteExistsParams.Add("CityId", cityId);
            var favoriteCount = await connection.ExecuteScalarAsync<int>(favoriteExistsQuery, favoriteExistsParams);

            if (favoriteCount > 0)
                return false; //not modified

            var insertCityFavoriteQuery = "INSERT INTO [CityFavorite] (UserId, CityId) VALUES (@UserId, @CityId);";
            var cityFavoriteParams = new DynamicParameters();
            cityFavoriteParams.Add("UserId", userId);
            cityFavoriteParams.Add("CityId", cityId);   

            var rowsAffected = await connection.ExecuteAsync(insertCityFavoriteQuery, cityFavoriteParams);
            return rowsAffected > 0;
        }

    }

    /// <summary>
    /// Unfavorites a city from a user, but leaves the original City record in place. 
    /// </summary>
    /// <param name="email"></param>
    /// <param name="city"></param>
    /// <param name="country"></param>
    /// <returns></returns>
    public async Task<bool> Unfavorite(string email, string city, string country)
    {
        using (var connection = base.CreateConnection())
        {
            var deleteQuery =
                @"DELETE cf
            FROM [CityFavorite] cf
            INNER JOIN [City] c on c.Id = cf.CityId
            INNER JOIN [User] u on u.Id = cf.UserId
            WHERE u.Email=@Email AND c.Name=@City AND c.Country=@Country";
            var deleteParams = new DynamicParameters();
            deleteParams.Add("Email", email);
            deleteParams.Add("City", city);
            deleteParams.Add("Country", country);

            var rowsDeleted = await connection.ExecuteAsync(deleteQuery, deleteParams);
            return rowsDeleted == 1;
        }
    }


    /// <summary>
    /// Gets a list of favorite cities for the user corresponding to email
    /// </summary>
    /// <param name="email"></param>
    /// <returns></returns>
    public async Task<IEnumerable<City>> GetFavorites(string email)
    { 
        using (var connection = base.CreateConnection())
        {
            var favoritesQuery = 
                @"SELECT c.Name, c.Country 
                FROM [City] c 
                INNER JOIN [CityFavorite] cf on c.Id = cf.CityId 
                INNER JOIN [User] u on u.Id = cf.UserId 
                WHERE u.Email=@Email";
            var favoritesParams = new DynamicParameters();
            favoritesParams.Add("Email", email);
            return await connection.QueryAsync<City>(favoritesQuery, favoritesParams);
        }
    }

}