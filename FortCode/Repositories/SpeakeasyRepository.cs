using System.Data;
using Dapper;
using FORTSpeakeasy.Models;

namespace FORTSpeakeasy.Repositories;

public class SpeakeasyRepository : RepositoryBase
{
    //if this project was larger, I'd consider wrapping some of this functionality into a service, but for the purpose of this test project I'd like to keep it simple
    private readonly UserRepository _userRepo;
    private readonly CityRepository _cityRepo;

    public SpeakeasyRepository(IConfiguration configuration, UserRepository userRepo, CityRepository cityRepo) : base(configuration) {
        _userRepo = userRepo;
        _cityRepo = cityRepo;
    }

    /// <summary>
    /// Creates the specified speakeasy if it does not already exist
    /// </summary>
    /// <param name="name"></param>
    /// <param name="cityId"></param>
    /// <returns></returns>
    public async Task<int> CreateSpeakeasy(string name, int cityId)
    {
        using (var connection = base.CreateConnection())
        {
            var speakeasyIdQuery = "SELECT s.Id FROM [Speakeasy] s WHERE s.Name=@Name and s.CityId=@CityId";
            var speakeasyIdParams = new DynamicParameters();
            speakeasyIdParams.Add("Name", name);
            speakeasyIdParams.Add("CityId", cityId);   

            var existingSpeakeasyId = await connection.ExecuteScalarAsync<int?>(speakeasyIdQuery, speakeasyIdParams);
            if (!existingSpeakeasyId.HasValue)
            {
                var insertSpeakeasyQuery = "INSERT INTO [Speakeasy] (Name, CityId) OUTPUT INSERTED.Id VALUES (@Name, @CityId);";
                existingSpeakeasyId = await connection.ExecuteScalarAsync<int>(insertSpeakeasyQuery, speakeasyIdParams);
            }

            return existingSpeakeasyId.Value;
        }

    }

    /// <summary>
    /// Sets the specified speakeasy as a favorite for user corresponding to email
    /// Will create the speakeasy and city if either does not exist
    /// </summary>
    /// <param name="email"></param>
    /// <param name="name"></param>
    /// <param name="favoriteDrink"></param>
    /// <param name="city"></param>
    /// <param name="country"></param>
    /// <returns></returns>
    public async Task<bool> Favorite(string email, string name, string favoriteDrink, string city, string country)
    { 
        using (var connection = base.CreateConnection())
        {
            var userId = await _userRepo.GetUserId(email);
            var cityId = await _cityRepo.CreateCity(city, country);
            var speakeasyId = await CreateSpeakeasy(name, cityId);

            var favoriteExistsQuery = 
                @"SELECT COUNT(1) 
                FROM [SpeakeasyFavorite] sf 
                WHERE sf.UserId=@UserId AND sf.SpeakeasyId=@SpeakeasyId";
            var favoriteExistsParams = new DynamicParameters();
            favoriteExistsParams.Add("UserId", userId);
            favoriteExistsParams.Add("SpeakeasyId", speakeasyId);
            var favoriteCount = await connection.ExecuteScalarAsync<int>(favoriteExistsQuery, favoriteExistsParams);

            var favoriteParams = new DynamicParameters();
            favoriteParams.Add("UserId", userId);
            favoriteParams.Add("SpeakeasyId", speakeasyId);   
            favoriteParams.Add("FavoriteDrink", favoriteDrink);   
            
            string upsertQuery; 

            if (favoriteCount == 0)
            {
                upsertQuery = "INSERT INTO [SpeakeasyFavorite] (UserId, SpeakeasyId, FavoriteDrink) VALUES (@UserId, @SpeakeasyId, @FavoriteDrink);";
            } 
            else 
            {
                upsertQuery = "UPDATE [SpeakeasyFavorite] SET FavoriteDrink=@FavoriteDrink WHERE UserId=@UserId AND SpeakeasyId=@SpeakeasyId;";
            }

            var rowsAffected = await connection.ExecuteAsync(upsertQuery, favoriteParams);
            return rowsAffected > 0;
        }

    }

    public async Task<bool> Unfavorite(string email, string name, string city, string country)
    {
        using (var connection = base.CreateConnection())
        {
            var deleteQuery =
                @"DELETE sf
            FROM [SpeakeasyFavorite] sf
            INNER JOIN [Speakeasy] s on s.Id = sf.SpeakeasyId
            INNER JOIN [City] c on c.Id = s.CityId
            INNER JOIN [User] u on u.Id = sf.UserId
            WHERE u.Email=@Email AND s.Name=@Speakeasy AND c.Name=@City AND c.Country=@Country";
            var deleteParams = new DynamicParameters();
            deleteParams.Add("Email", email);
            deleteParams.Add("Speakeasy", name);
            deleteParams.Add("City", city);
            deleteParams.Add("Country", country);

            var rowsDeleted = await connection.ExecuteAsync(deleteQuery, deleteParams);
            return rowsDeleted == 1;
        }
    }


    /// <summary>
    /// Gets a list of favorite speakeasies for the user corresponding to email
    /// City and Country are not present in response, consider adding them or chaning the model
    /// </summary>
    /// <param name="email"></param>
    /// <param name="city"></param>
    /// <param name="country"></param>
    /// <returns></returns>
    public async Task<IEnumerable<Speakeasy>> GetFavorites(string email, string city, string country)
    { 
        using (var connection = base.CreateConnection())
        {
            var favoritesQuery = 
                @"SELECT s.Name, sf.FavoriteDrink 
                FROM [Speakeasy] s 
                INNER JOIN [SpeakeasyFavorite] sf on s.Id = sf.SpeakeasyId 
                INNER JOIN [City] c on c.Id = s.CityId 
                INNER JOIN [User] u on u.Id = sf.UserId 
                WHERE u.Email=@Email AND c.Name=@Name AND c.Country=@Country";
            var favoritesParams = new DynamicParameters();
            favoritesParams.Add("Email", email);
            favoritesParams.Add("Name", city);
            favoritesParams.Add("Country", country);  
            return await connection.QueryAsync<Speakeasy>(favoritesQuery, favoritesParams);
        }
    }

    /// <summary>
    /// Gets a list of favorite speakeasies which are also favorite speakeasies for other users
    /// Along with a count of how many other users have favorited each speakeasy
    /// </summary>
    /// <param name="email"></param>
    /// <returns></returns>
    public async Task<IEnumerable<FavoriteSpeakeasy>> GetSharedFavorites(string email)
    {
        using (var connection = base.CreateConnection())
        {
            var userId = await _userRepo.GetUserId(email);

            var sharedFavoritesQuery = 
                @"SELECT s.Name, COUNT(1)-1 'Count'
                FROM [Speakeasy] s
                INNER JOIN [SpeakeasyFavorite] sf on s.Id = sf.SpeakeasyId and sf.UserId = @UserId
                INNER JOIN [SpeakeasyFavorite] shared on sf.SpeakeasyId = shared.SpeakeasyId
                GROUP BY s.Name
                HAVING COUNT(1) > 1";
            var sharedFavoritesParams = new DynamicParameters();
            sharedFavoritesParams.Add("UserId", userId);
            return await connection.QueryAsync<FavoriteSpeakeasy>(sharedFavoritesQuery, sharedFavoritesParams);
        }
    }


}