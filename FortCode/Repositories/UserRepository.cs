using System.Data;
using Dapper;

namespace FORTSpeakeasy.Repositories;

public class UserRepository : RepositoryBase
{
    public UserRepository(IConfiguration configuration) : base(configuration) {

    }

    /// <summary>
    /// Creates the specified user, will fail if the user already exists
    /// </summary>
    /// <param name="name"></param>
    /// <param name="email"></param>
    /// <param name="password"></param>
    /// <returns></returns>
    public async Task<int> Create(string name, string email, string password)
    {
        string hashedPassword = BCrypt.Net.BCrypt.HashPassword(password);

        var query = "INSERT INTO [User] (Name, Email, Password) OUTPUT INSERTED.Id VALUES (@Name, @Email, @Password);";
        var parameters = new DynamicParameters();
        parameters.Add("Name", name);
        parameters.Add("Email", email);
        parameters.Add("Password", hashedPassword);
    
        using (var connection = base.CreateConnection())
        {
            return await connection.ExecuteScalarAsync<int>(query, parameters);
        }
    }

    /// <summary>
    /// Verifies if the given password is correct for the specified email address
    /// </summary>
    /// <param name="email"></param>
    /// <param name="password"></param>
    /// <returns></returns>
    public async Task<bool> Authenticate(string email, string password)
    {
        var query = "SELECT u.Password FROM [User] u WHERE u.Email=@Email";
        var parameters = new DynamicParameters();
        parameters.Add("Email", email);
        
        using (var connection = base.CreateConnection())
        {
            var hashedPassword = await connection.ExecuteScalarAsync<string>(query, parameters);
            if (hashedPassword == null)
                return false;

            return BCrypt.Net.BCrypt.Verify(password, hashedPassword);
        }
    }

    /// <summary>
    /// Resolves email address to UserId
    /// </summary>
    /// <param name="email"></param>
    /// <returns></returns>
    public async Task<int> GetUserId(string email)
    {
        var query = "SELECT u.Id FROM [User] u WHERE u.Email=@Email";
        var parameters = new DynamicParameters();
        parameters.Add("Email", email);
        
        using (var connection = base.CreateConnection())
        {
           return await connection.ExecuteScalarAsync<int>(query, parameters);
        }
    }
}