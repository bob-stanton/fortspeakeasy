using Dapper;
using Microsoft.Data.SqlClient;

namespace FORTSpeakeasy.Repositories;

public class AdminRepository : RepositoryBase
{
    private readonly string _masterConnectionString;

    public AdminRepository(IConfiguration configuration) : base(configuration) {
        // Sort of hacky but this seems to be the easiest way to get a connection string for master 
       _masterConnectionString = configuration.GetConnectionString("DbContext").Replace("FortCodeDb", "master");
    }

    /// <summary>
    /// Create the FortCodeDb database
    /// Will drop database if it already exists
    /// This should be safe for production, right? RIGHT?
    /// </summary>
    /// <returns>-1</returns>
    public async Task<int> CreateDatabase()
    {
        using (var connection = new SqlConnection(_masterConnectionString))
        {
            var query = 
            @"IF EXISTS(SELECT * FROM sys.databases WHERE name = 'FortCodeDb') BEGIN
                ALTER DATABASE FortCodeDb SET single_user WITH ROLLBACK IMMEDIATE;
                DROP DATABASE FortCodeDb;
            END
            CREATE DATABASE FortCodeDb;"; 

            var rowsAffected = await connection.ExecuteAsync(query);
            return rowsAffected;
        }
    }

    /// <summary>
    /// Creates the User, City, Speakeasy, CityFavorite and SpeakeasyFavorite tables. 
    /// Will drop existing tables if they exist. 
    /// </summary>
    /// <returns>-1</returns>
    public async Task<int> CreateTables()
    {
        using (var connection = base.CreateConnection())
        {
            var query = 
            @"
IF OBJECT_ID('dbo.SpeakeasyFavorite', 'U') IS NOT NULL 
    DROP TABLE [SpeakeasyFavorite]
IF OBJECT_ID('dbo.CityFavorite', 'U') IS NOT NULL 
    DROP TABLE [CityFavorite]
IF OBJECT_ID('dbo.User', 'U') IS NOT NULL 
    DROP TABLE [User]
IF OBJECT_ID('dbo.Speakeasy', 'U') IS NOT NULL 
    DROP TABLE [Speakeasy]
IF OBJECT_ID('dbo.City', 'U') IS NOT NULL 
    DROP TABLE [City]

CREATE TABLE [User] ([Id] INT IDENTITY(1, 1) NOT NULL PRIMARY KEY, [Name] NVARCHAR(1000) NOT NULL, [Email] NVARCHAR(500) NOT NULL, [Password] VARCHAR(500) NOT NULL);
CREATE UNIQUE INDEX IX_User_Email
ON [User]([Email] ASC)

CREATE TABLE [City] ([Id] INT IDENTITY(1, 1) NOT NULL PRIMARY KEY, [Name] nvarchar(1000) NOT NULL, [Country] nvarchar(1000) NOT NULL);
CREATE TABLE [CityFavorite] ([UserId] INT NOT NULL FOREIGN KEY REFERENCES [User]([Id]), [CityId] INT NOT NULL FOREIGN KEY REFERENCES [City]([Id]));
CREATE TABLE [Speakeasy] ([Id] INT IDENTITY(1, 1) NOT NULL PRIMARY KEY, [Name] nvarchar(1000) NOT NULL, [CityId] INT NOT NULL FOREIGN KEY REFERENCES [City]([Id]));
CREATE TABLE [SpeakeasyFavorite] ([UserId] INT NOT NULL FOREIGN KEY REFERENCES [User]([Id]), [SpeakeasyId] INT NOT NULL FOREIGN KEY REFERENCES [Speakeasy]([Id]), [FavoriteDrink] nvarchar(1000) NOT NULL);
"; 

            var rowsAffected = await connection.ExecuteAsync(query);
            return rowsAffected;
        }
    }
    
}