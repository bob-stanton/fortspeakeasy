using System.Data;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Dapper;

namespace FORTSpeakeasy.Repositories;

/// <summary>
/// Simple base class for Repositories
/// Initialized database connection
/// </summary>
public class RepositoryBase
{

    public RepositoryBase(IConfiguration configuration) {
        _configuration = configuration;
    }

    private readonly IConfiguration _configuration;

    protected IDbConnection CreateConnection() 
    {
       return new SqlConnection(_configuration.GetConnectionString("DbContext"));
    }

}