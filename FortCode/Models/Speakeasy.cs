namespace FORTSpeakeasy.Models;

public class Speakeasy
{
    public City City { get; set; }
    public string Name { get; set; }
    public string FavoriteDrink { get; set; }
}