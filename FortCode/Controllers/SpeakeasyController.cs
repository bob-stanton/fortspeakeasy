using Ardalis.GuardClauses;
using Microsoft.AspNetCore.Mvc;
using FORTSpeakeasy.Models;
using FORTSpeakeasy.Repositories;

namespace FORTSpeakeasy.Controllers;

[ApiController]
public class SpeakeasyController : ControllerBase
{
    private readonly UserContext _user;

    private readonly SpeakeasyRepository _speakeasyRepo;

    public SpeakeasyController(UserContext user, SpeakeasyRepository speakeasyRepo)
    {
        _user = user;
        _speakeasyRepo = speakeasyRepo;
    }

    /// <summary>
    /// Adds the specified speakeasy as a favorite for the authorized user.
    /// If the city does not exist, it will be created
    /// </summary>
    /// <param name="speakeasyName"></param>
    /// <param name="favoriteDrink"></param>
    /// <param name="cityName"></param>
    /// <param name="countryName"></param>
    /// <returns></returns>
    [HttpPost("/api/cities/{cityName}/{countryName}/speakeasy/{speakeasyName}/favorite")]
    
    public async Task<IActionResult> Favorite(string speakeasyName, [FromForm]string favoriteDrink, string cityName, string countryName)
    {
        Guard.Against.NullOrWhiteSpace(speakeasyName, nameof(speakeasyName));
        Guard.Against.NullOrWhiteSpace(favoriteDrink, nameof(favoriteDrink));
        Guard.Against.NullOrWhiteSpace(cityName, nameof(cityName));
        Guard.Against.NullOrWhiteSpace(countryName, nameof(countryName));

        var success = await _speakeasyRepo.Favorite(_user.Email, speakeasyName, favoriteDrink, cityName, countryName);

        if (success)
            return Ok();
        
        return new StatusCodeResult(StatusCodes.Status304NotModified);
    }

    /// <summary>
    /// Removes a speakeasy as a favorite. Leaves the original speakeasy record
    /// </summary>
    /// <param name="speakeasyName"></param>
    /// <param name="city"></param>
    /// <param name="country"></param>
    /// <returns></returns>
    [HttpDelete("/api/cities/{city}/{country}/speakeasy/{speakeasyName}/favorite")]
    public async Task<IActionResult> Unfavorite(string speakeasyName, string city, string country)
    {
        Guard.Against.NullOrWhiteSpace(speakeasyName, nameof(speakeasyName));
        Guard.Against.NullOrWhiteSpace(city, nameof(city));
        Guard.Against.NullOrWhiteSpace(country, nameof(country));

        var success = await _speakeasyRepo.Unfavorite(_user.Email, speakeasyName, city, country);

        if (success)
            return Ok();
        
        return new StatusCodeResult(StatusCodes.Status304NotModified);
    }

    /// <summary>
    /// Gets the current user's list of favorite speakeasies for the specified city
    /// </summary>
    /// <param name="cityName"></param>
    /// <param name="countryName"></param>
    /// <returns></returns>
    [HttpGet("/api/cities/{cityName}/{countryName}/speakeasies/favorites")]
    public async Task<IEnumerable<Speakeasy>> GetFavorites(string cityName, string countryName)
    {
        Guard.Against.NullOrWhiteSpace(cityName, nameof(cityName));
        Guard.Against.NullOrWhiteSpace(countryName, nameof(countryName));

        return await _speakeasyRepo.GetFavorites(_user.Email, cityName, countryName);
    }
    
    /// <summary>
    /// Gets the current user's list of favorite speakeasies which are also favorited by other users
    /// </summary>
    /// <returns></returns>
    [HttpGet("/api/speakeasy/sharedFavorites")]
    public async Task<IEnumerable<FavoriteSpeakeasy>> SharedFavorites()
    {
        return await _speakeasyRepo.GetSharedFavorites(_user.Email);
    }
}
