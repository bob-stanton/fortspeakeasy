using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;

using FORTSpeakeasy.Repositories;

namespace FORTSpeakeasy.Controllers;

[ApiController]
public class AdminController : ControllerBase
{
    private readonly AdminRepository _adminRepo;
    private readonly UserRepository _userRepo;
    private readonly CityRepository _cityRepo;
    private readonly SpeakeasyRepository _speakeasyRepo;


    public AdminController(AdminRepository adminRepo, UserRepository userRepo, CityRepository cityRepo, SpeakeasyRepository speakeasyRepo)
    {
        _adminRepo = adminRepo;
        _userRepo = userRepo;
        _cityRepo = cityRepo;
        _speakeasyRepo = speakeasyRepo;
    }

    /// <summary>
    /// Create the FortCodeDb database
    /// Will drop database if it already exists
    /// </summary>
    /// <returns statusCode="200">-1</returns>
    [HttpPost("/api/admin/database/create")]
    [AllowAnonymous]
    public async Task<int> CreateDatabase()
    {
        return await _adminRepo.CreateDatabase();
    }

    /// <summary>
    /// Creates the User, City, Speakeasy, CityFavorite and SpeakeasyFavorite tables. 
    /// Will drop existing tables if they exist. 
    /// </summary>
    /// <returns statusCode="200">-1</returns>
    [HttpPost("/api/admin/database/tables/create")]
    [AllowAnonymous]
    public async Task<int> CreateTables()
    {
        return await _adminRepo.CreateTables();
    }


    /// <summary>
    /// Seeds tables with sample Users, Cities, Speakeasies and Favorites for each. 
    /// </summary>
    /// <returns></returns>
    [HttpPost("/api/admin/database/seed")]
    [AllowAnonymous]
    public async Task<IActionResult> Seed()
    {
        await _userRepo.Create("Bob", "bob@email.com", "Password1");
        await _userRepo.Create("Tom", "tom@email.com", "Password2");
        await _userRepo.Create("Joe", "joe@email.com", "Password3");
        await _userRepo.Create("Mike", "mike@email.com", "Password4");
        
        var keeneId = await _cityRepo.CreateCity("Keene, NH", "USA");
        var nycId = await _cityRepo.CreateCity("New York, NY", "USA");
        var bostonId = await _cityRepo.CreateCity("Boston, MA", "USA");
        var phillyId = await _cityRepo.CreateCity("Philadelphia, PA", "USA");

        await _cityRepo.Favorite("bob@email.com", "Keene, NH", "USA");
        await _cityRepo.Favorite("bob@email.com", "New York, NY", "USA");
        await _cityRepo.Favorite("bob@email.com", "Philadelphia, PA", "USA");
        await _cityRepo.Favorite("tom@email.com", "New York, NY", "USA");
        await _cityRepo.Favorite("joe@email.com", "Philadelphia, PA", "USA");
        await _cityRepo.Favorite("mike@email.com", "Philadelphia, PA", "USA");

        var brewbakersId = await _speakeasyRepo.CreateSpeakeasy("Brewbakers Café", keeneId);
        var unionOysterId = await _speakeasyRepo.CreateSpeakeasy("Union Oyster House", bostonId);
        var abigalsId = await _speakeasyRepo.CreateSpeakeasy("Abigail's Tea Room", bostonId);
        var russianTeaRoomId = await _speakeasyRepo.CreateSpeakeasy("The Russian Tea Room", nycId);
        var bibbleAndSipId = await _speakeasyRepo.CreateSpeakeasy("Bibble & Sip", nycId);
        var dalessandrosId = await _speakeasyRepo.CreateSpeakeasy("Dalessandro's Steaks", phillyId);

        await _speakeasyRepo.Favorite("bob@email.com", "Brewbakers Café", "Honey Lemon Ginger", "Keene, NH", "USA");
        await _speakeasyRepo.Favorite("bob@email.com", "Union Oyster House", "Water", "Boston, MA", "USA");
        await _speakeasyRepo.Favorite("bob@email.com", "Abigail's Tea Room", "Abigail'’'s Blend", "Boston, MA", "USA");
        await _speakeasyRepo.Favorite("bob@email.com", "Bibble & Sip", "Matcha Jasmine Latte", "New York, NY", "USA");
        //a cheesesteak is basically a drink if you don't think about it too hard, its gotta be like 80% water anyway... 
        await _speakeasyRepo.Favorite("bob@email.com", "Dalessandro's Steaks", "Cheesesteak", "Philadelphia, PA", "USA");
        await _speakeasyRepo.Favorite("tom@email.com", "Dalessandro's Steaks", "Cheesesteak", "Philadelphia, PA", "USA");
        await _speakeasyRepo.Favorite("joe@email.com", "Dalessandro's Steaks", "Cheesesteak", "Philadelphia, PA", "USA");
        await _speakeasyRepo.Favorite("mike@email.com", "Dalessandro's Steaks", "Cheesesteak", "Philadelphia, PA", "USA");
        await _speakeasyRepo.Favorite("mike@email.com", "Bibble & Sip", "Americano", "New York, NY", "USA");
        
        return Ok();
    }

}
