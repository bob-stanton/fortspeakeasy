using Ardalis.GuardClauses;
using System.Security.Claims;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using Microsoft.AspNetCore.Authorization;
using FORTSpeakeasy.Repositories;
using System.Security.Authentication;

namespace FORTSpeakeasy.Controllers;

[ApiController]
public class UserController : ControllerBase
{
    private readonly UserRepository _userRepo;

    public UserController(UserRepository userRepo)
    {

        _userRepo = userRepo;
    }

    /// <summary>
    /// Creates a new user which is implicitly authorized to access other APIs.
    /// Throws an exception if email already exists
    /// </summary>
    /// <param name="name">Name for user (not really used elsewhere, limit 1,000 characters)</param>
    /// <param name="email">Email Address for user (limit 500 characters)</param>
    /// <param name="password">Password for user</param>
    /// <returns></returns>
    [HttpPost("/api/user")]
    [AllowAnonymous]
    public async Task<int> CreateNewUser([FromForm]string name, [FromForm]string email, [FromForm]string password)
    {
        Guard.Against.NullOrWhiteSpace(name, nameof(name));
        Guard.Against.NullOrWhiteSpace(email, nameof(email));
        Guard.Against.NullOrWhiteSpace(password, nameof(password));

        return await _userRepo.Create(name, email, password);
    }

    /// <summary>
    /// Generates a JWT Bearer token for use with other APIs
    /// </summary>
    /// <param name="email">Email Address of user to authorize</param>
    /// <param name="password">Password for above user</param>
    /// <returns></returns>
    [HttpPost("/api/user/authenticate")]
    [AllowAnonymous]
    public async Task<IActionResult> Authenticate([FromForm]string email, [FromForm]string password)
    {
        Guard.Against.NullOrWhiteSpace(email, nameof(email));
        Guard.Against.NullOrWhiteSpace(password, nameof(password));

        //it might be a good TODO to add some sort of throttling or rate limiting here

        if (await _userRepo.Authenticate(email, password))
        {
            return new ObjectResult(GenerateToken(email));
        }

        return Unauthorized();
    }

    private dynamic GenerateToken(string email)
    {
        var claims = new List<Claim>
        {
            new Claim(ClaimTypes.Email, email)
        };

        var token = new JwtSecurityToken(
            new JwtHeader(
                new SigningCredentials(
                    SecretKey.Key, 
                    SecurityAlgorithms.HmacSha256
                )
            ), 
            new JwtPayload(claims)
        );

        return new
        {
            Access_Token = new JwtSecurityTokenHandler().WriteToken(token)
        };
    }
}
