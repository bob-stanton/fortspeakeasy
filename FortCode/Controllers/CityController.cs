using Ardalis.GuardClauses;
using Microsoft.AspNetCore.Mvc;
using FORTSpeakeasy.Models;
using FORTSpeakeasy.Repositories;

namespace FORTSpeakeasy.Controllers;

[ApiController]
public class CityController : ControllerBase
{
    private readonly UserContext _user;

    private readonly CityRepository _cityRepo;

    public CityController(UserContext user, CityRepository cityRepo)
    {
        _user = user;

        _cityRepo = cityRepo;
    }

    /// <summary>
    /// Adds the specified city as a favorite for the authorized user
    /// </summary>
    /// <param name="city">Name of the City to favorite</param>
    /// <param name="country">Name of the Country for the specified City</param>
    /// <returns></returns>
    [HttpPost("/api/cities/{city}/{country}/favorite")]
    public async Task<IActionResult> Favorite(string city, string country)
    {
        Guard.Against.NullOrWhiteSpace(city, nameof(city));
        Guard.Against.NullOrWhiteSpace(country, nameof(country));

        var success = await _cityRepo.Favorite(_user.Email, city, country);

        if (success)
            return Ok();
        
        return new StatusCodeResult(StatusCodes.Status304NotModified);
    }

    /// <summary>
    /// Removes a city as a favorite. Leaves the original city record
    /// </summary>
    /// <param name="city"></param>
    /// <param name="country"></param>
    /// <returns></returns>
    [HttpDelete("/api/cities/{city}/{country}/favorite")]
    public async Task<IActionResult> Unfavorite(string city, string country)
    {
        Guard.Against.NullOrWhiteSpace(city, nameof(city));
        Guard.Against.NullOrWhiteSpace(country, nameof(country));

        var success = await _cityRepo.Unfavorite(_user.Email, city, country);

        if (success)
            return Ok();
        
        return new StatusCodeResult(StatusCodes.Status304NotModified);
    }

    /// <summary>
    /// Gets the current user's favorite cities
    /// </summary>
    /// <returns></returns>
    [HttpGet("/api/cities/favorites")]
    public async Task<IEnumerable<City>> GetFavorites()
    {
        return await _cityRepo.GetFavorites(_user.Email);
    }
}
