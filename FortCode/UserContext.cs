using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;

public class UserContext
{
    /// <summary>
    /// Extracts Email Address from JWT
    /// </summary>
    /// <param name="context"></param>
    public UserContext(IHttpContextAccessor context)
    {
        var claimsIdentity = context.HttpContext!.User.Identity as ClaimsIdentity;
        _email = claimsIdentity!.FindFirst(ClaimTypes.Email)!.Value;
    }

    private readonly string _email;
    public string Email 
    {
       get { return _email; }
    }

}