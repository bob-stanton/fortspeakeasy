using System.Text;
using Microsoft.IdentityModel.Tokens;

public static class SecretKey
{
    public static SymmetricSecurityKey Key = new SymmetricSecurityKey(
        Encoding.UTF8.GetBytes("in a real project you'd want to import a secret here instead of this")
    );
}